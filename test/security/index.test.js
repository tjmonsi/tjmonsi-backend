/**
 * @module
 * @description Security Test template
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import tap from 'tap';
import { Security } from '../../src/security/index.js';
import 'must/register.js';

tap.mochaGlobals();

describe('Security', async () => {
  let security;
  before(async () => {
    security = new Security();
  });

  it('bearerAuth should work', async () => {
    // mocks request and response
    const request = {};
    const response = {};
    security.bearerAuth(request, response);
  });
});
