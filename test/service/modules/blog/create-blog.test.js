/**
 * @module
 * @description Create blog test service test case
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import tap from 'tap';
import { createBlog } from '../../../../src/service/modules/blog/create-blog.js';
import { firestore } from '../../../../src/utils/firestore/index.js';
import { getConfig } from '../../../../src/utils/helpers/config.js';
import 'must/register.js';

tap.mochaGlobals();

const { google } = getConfig();
const { firestore: firestoreCollections } = google;
const { blog: blogCollectionName } = firestoreCollections;

describe('createBlog operationId', async () => {
  it('should work', async () => {
    /** @type {*} */
    const request = {
      body: {
        title: 'A new Blog',
        summary: 'This is a new Blog',
        text: 'This is a new Blog text'
      }
    };

    // mocks request and response
    const response = await createBlog(request);
    response.success.must.be.true();

    response.data.must.exist();

    response.data.title.must.be.equal(request.body.title);
    response.data.summary.must.be.equal(request.body.summary);
    response.data.text.must.be.equal(request.body.text);

    // get the id
    const { blogId } = response.data;

    // check in firestore if the id exists

    const blogSnap = await firestore.doc(`${blogCollectionName}/${blogId}`).get();
    const blogData = blogSnap.data();

    blogData.title.must.be.equal(response.data.title);
    blogData.summary.must.be.equal(response.data.summary);
    blogData.text.must.be.equal(response.data.text);
    blogData.createDate.toMillis().must.be.equal(response.data.createDate);
    blogData.updateDate.toMillis().must.be.equal(response.data.updateDate);
  });
});
