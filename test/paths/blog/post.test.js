/**
 * @module
 * @description Test for the path POST /blog
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import tap from 'tap';
import { build } from '../../../src/app.js';
import 'must/register.js';

tap.mochaGlobals();

describe('If I go for POST /blog, I will create a blog object', async () => {
  let app;
  before(async () => {
    app = await build();
  });

  it('Should be able to get { success: true, data: BlogData }', async () => {
    const request = {
      body: {
        title: 'A new Blog',
        summary: 'This is a new Blog',
        text: 'This is a new Blog text'
      }
    };

    const response = await app.inject({
      method: 'POST',
      url: 'dev/blog',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(request.body)
    });

    response.statusCode.must.be.equal(200);

    const body = await response.json();
    const { success, data } = body;

    success.must.be.true();
    data.must.exist();

    data.title.must.be.equal(request.body.title);
    data.summary.must.be.equal(request.body.summary);
    data.text.must.be.equal(request.body.text);
    data.createDate.must.exist();
    data.updateDate.must.exist();
  });

  after(async () => {
    app.close();
  });
});
