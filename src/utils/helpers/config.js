/**
 * @module
 * @description Configuration loader file
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { readFileSync } from 'fs';
const opts = JSON.parse(readFileSync('src/config.json', 'utf8'));

export const getConfig = () => {
  const env = process.env.NODE_ENV || 'development';
  // @ts-ignore
  const environment = opts.environment[`${env}`];

  /** @type {*} */
  const config = {};

  for (const key in environment) {
    config[`${key}`] = {
      ...config[`${key}`],
      ...environment[`${key}`]
    };
  }

  return config;
};
