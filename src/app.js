/**
 * @module
 * @description Starting app file for Fastify
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Fastify from 'fastify';
import openApiGlue from 'fastify-openapi-glue';
import swagger from 'fastify-swagger';
import sensible from 'fastify-sensible';
import { specification } from './specification/index.js';
import { Service } from './service/index.js';
import { Security } from './security/index.js';
import { errorHandler } from './utils/helpers/error-handler.js';

/**
 * This is the function to build the app
 *
 * @param {*} opts this is an option set used to configure Fastify. See Fastify options
 * @returns {Promise<import('fastify').FastifyInstance<import('http2').Http2SecureServer, import('http2').Http2ServerRequest, import('http2').Http2ServerResponse, import('fastify').FastifyLoggerInstance>>}
 */
export const build = async (opts = {}) => {
  const fastify = Fastify(opts);

  await fastify.register(sensible);
  // @ts-ignore
  fastify.setErrorHandler(errorHandler);

  const service = new Service();
  const securityHandlers = new Security();

  /**
   * @type {import('fastify').FastifyRegisterOptions<import('fastify-openapi-glue').FastifyOpenapiGlueOptions>}
   */
  const options = {
    specification,
    service,
    securityHandlers,
    /* c8 ignore next */
    prefix: process.env.NODE_ENV === 'production' ? '' : 'dev',
    noAdditional: true
  };

  /**
   * @type {import('fastify').FastifyRegisterOptions<import('fastify-swagger').FastifyDynamicSwaggerOptions>}
   */

  const swaggerOptions = {
    // @ts-ignore
    openapi: specification,
    routePrefix: '/docs',
    exposeRoute: process.env.NODE_ENV !== 'production'
  };

  fastify.register(swagger, swaggerOptions);
  fastify.register(openApiGlue, options);

  return fastify;
};
