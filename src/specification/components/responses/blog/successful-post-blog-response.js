/**
 * @module
 * @description This is the response for a create Blog function call
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { schemas } from '../../../../utils/open-api/components.js';

export const SuccessfulPostBlogResponse = {
  description: 'Successful Creation of a blog',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        properties: {
          success: {
            $ref: `${schemas}/Success`
          },
          data: {
            $ref: `${schemas}/Blog`
          }
        }
      }
    }
  }
};
