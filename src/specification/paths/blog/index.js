/**
 * @module
 * @description This is the path definition for blog route
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { requestBodies, responses } from '../../../utils/open-api/components.js';

export const blog = {
  get: {
    summary: 'Get Many Blog',
    description: 'Get a list of blog items',
    operationId: 'getManyBlog',
    tags: [
      'Blog'
    ],
    parameters: [
      {
        name: 'limit',
        in: 'query',
        description: 'limit of the blog returns',
        schema: {
          type: 'number',
          example: 10
        }
      },
      {
        name: 'nextPage',
        in: 'query',
        description: 'The next ID for our pages',
        schema: {
          type: 'string',
          example: 'exampleId'
        }
      },
      {
        name: 'prevPage',
        in: 'query',
        description: 'The next ID for our pages',
        schema: {
          type: 'string',
          example: 'exampleIdPage'
        }
      }
    ],
    responses: {
      200: {
        $ref: `${responses}/SuccessfulGetManyBlogResponse`
      }
    }
  },
  post: {
    summary: 'Create Blog',
    description: 'Create one blog item',
    operationId: 'createBlog',
    tags: [
      'Blog'
    ],
    requestBody: {
      $ref: `${requestBodies}/CreateBlogRequest`
    },
    responses: {
      200: {
        $ref: `${responses}/SuccessfulPostBlogResponse`
      }
    }
  }
};
