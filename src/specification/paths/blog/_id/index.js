/**
 * @module
 * @description This is for routes for /blog/:id
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { requestBodies, responses } from '../../../../utils/open-api/components.js';

export const blogId = {
  get: {
    summary: 'Get One Blog',
    description: 'Get one blog item',
    operationId: 'getBlog',
    tags: [
      'Blog'
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        description: 'ID of the blog',
        schema: {
          type: 'string',
          example: 'exampleID'
        },
        required: true
      }
    ],
    responses: {
      200: {
        $ref: `${responses}/SuccessfulPostBlogResponse`
      }
    }
  },
  put: {
    summary: 'Update One Blog',
    description: 'Update one blog item',
    operationId: 'updateBlog',
    tags: [
      'Blog'
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        description: 'ID of the blog',
        schema: {
          type: 'string',
          example: 'exampleID'
        },
        required: true
      }
    ],
    requestBody: {
      $ref: `${requestBodies}/UpdateBlogRequest`
    },
    responses: {
      200: {
        $ref: `${responses}/SuccessfulPostBlogResponse`
      }
    }
  },
  delete: {
    summary: 'Delete One Blog',
    description: 'Delete one blog item',
    operationId: 'deleteBlog',
    tags: [
      'Blog'
    ],
    parameters: [
      {
        name: 'id',
        in: 'path',
        description: 'ID of the blog',
        schema: {
          type: 'string',
          example: 'exampleID'
        },
        required: true
      }
    ],
    responses: {
      200: {
        $ref: `${responses}/SuccessfulResponse`
      }
    }
  }
};
