/**
 * @module
 * @description Function to Create a blog
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FieldValue } from '@google-cloud/firestore';
import { firestore } from '../../../utils/firestore/index.js';
import { getConfig } from '../../../utils/helpers/config.js';

const { google } = getConfig();
const { firestore: firestoreCollections } = google;
const { blog: blogCollectionName } = firestoreCollections;

/**
 * This is the createBlog service function
 *
 * @param {import('fastify').FastifyRequest} request
 */
export const createBlog = async (request) => {
  const { body } = request;
  const { title, summary, text } = /** @type {*} */(body);

  const data = {
    title,
    summary,
    text
  };

  const blogCollection = firestore.collection(blogCollectionName);

  const blogDoc = await blogCollection.add({
    ...data,
    createDate: FieldValue.serverTimestamp(),
    updateDate: FieldValue.serverTimestamp()
  });

  const blogSnap = await blogDoc.get();
  /** @type {*} */
  const newData = blogSnap.data();

  newData.createDate = newData.createDate.toMillis();
  newData.updateDate = newData.updateDate.toMillis();

  return {
    success: true,
    data: {
      ...newData,
      blogId: blogDoc.id
    }
  };
};
