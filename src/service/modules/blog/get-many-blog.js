/**
 * @module
 * @description Get Many Blog Operation ID
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { firestore } from '../../../utils/firestore/index.js';
import { getConfig } from '../../../utils/helpers/config.js';

const { google } = getConfig();
const { firestore: firestoreCollections } = google;
const { blog: blogCollectionName } = firestoreCollections;

/**
* This is the getManyBlog service function
*
* @param {import('fastify').FastifyRequest} request
*/
export const getManyBlog = async (request) => {
  const { query } = request;
  const { limit = 10, nextPage, prevPage } = /** @type {*} */(query);

  /** @type {*} */
  const data = [];

  const blogCollection = firestore.collection(blogCollectionName);

  let startSnap = blogCollection
    .orderBy('createDate', 'desc');

  if (nextPage) {
    const snapshot = await firestore.doc(`${blogCollectionName}/${nextPage}`).get();
    if (snapshot.exists) {
      startSnap = startSnap.startAfter(snapshot);
    }
  }

  if (prevPage) {
    const snapshot = await firestore.doc(`${blogCollectionName}/${prevPage}`).get();
    if (snapshot.exists) {
      startSnap = startSnap.endBefore(snapshot);
    }
  }

  const blogSnap = await startSnap.limit(limit).get();

  blogSnap.docs.forEach(blogData => {
    const newData = blogData.data();

    newData.createDate = newData.createDate.toMillis();
    newData.updateDate = newData.updateDate.toMillis();

    data.push({
      ...newData,
      blogId: blogData.id
    });
  });

  return {
    success: true,
    data
  };
};
