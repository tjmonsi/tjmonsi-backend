/**
 * @module
 * @description Get blog service
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { firestore } from '../../../utils/firestore/index.js';
import { getConfig } from '../../../utils/helpers/config.js';

const { google } = getConfig();
const { firestore: firestoreCollections } = google;
const { blog: blogCollectionName } = firestoreCollections;

/**
* This is the getManyBlog service function
*
* @param {import('fastify').FastifyRequest} request
* @param {import('fastify').FastifyReply<Response>} response
*/
export const deleteBlog = async (request, response) => {
  const { params } = request;
  const { id: blogId } = /** @type {*} */(params);

  const blogDoc = await firestore.doc(`${blogCollectionName}/${blogId}`).get();

  if (!blogDoc.exists) {
    return response.notFound();
  }

  await firestore.doc(`${blogCollectionName}/${blogId}`).delete();

  return {
    success: true
  };
};
