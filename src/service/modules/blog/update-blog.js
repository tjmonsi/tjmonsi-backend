/**
 * @module
 * @description Update blog service
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { FieldValue } from '@google-cloud/firestore';
import { firestore } from '../../../utils/firestore/index.js';
import { getConfig } from '../../../utils/helpers/config.js';

const { google } = getConfig();
const { firestore: firestoreCollections } = google;
const { blog: blogCollectionName } = firestoreCollections;

/**
* This is the getManyBlog service function
*
* @param {import('fastify').FastifyRequest} request
* @param {import('fastify').FastifyReply<Response>} response
*/
export const updateBlog = async (request, response) => {
  const { params, body } = request;
  const { id: blogId } = /** @type {*} */(params);
  const { title, summary, text } = /** @type {*} */(body);

  const blogRef = firestore.doc(`${blogCollectionName}/${blogId}`);

  let blogDoc = await blogRef.get();

  // check if it exists
  if (!blogDoc.exists) {
    return response.notFound();
  }

  const data = {};

  if (title) {
    data.title = title;
  }

  if (summary) {
    data.summary = summary;
  }

  if (text) {
    data.text = text;
  }

  if (title || summary || text) {
    await blogRef.update({
      ...data,
      updateDate: FieldValue.serverTimestamp()
    });
    blogDoc = await blogRef.get();
  }

  /** @type {*} */
  const newData = blogDoc.data();

  newData.createDate = newData.createDate.toMillis();
  newData.updateDate = newData.updateDate.toMillis();

  return {
    success: true,
    data: {
      ...newData,
      blogId: blogDoc.id
    }
  };
};
