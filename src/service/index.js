/**
 * @module
 * @description Service index
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { home } from './modules/general/home.js';
import { createBlog } from './modules/blog/create-blog.js';
import { getManyBlog } from './modules/blog/get-many-blog.js';
import { getBlog } from './modules/blog/get-blog.js';
import { updateBlog } from './modules/blog/update-blog.js';
import { deleteBlog } from './modules/blog/delete-blog.js';

export class Service {
  home = home
  createBlog = createBlog
  getManyBlog = getManyBlog
  getBlog = getBlog
  updateBlog = updateBlog
  deleteBlog = deleteBlog
}
