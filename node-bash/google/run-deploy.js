/**
 * @module
 * @description Deploy Cloud Run
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getConfig } from '../../src/utils/helpers/config.js';

const { google } = getConfig();
const { projectId, tag, region, allowUnauthenticated, serviceAccount } = google;

console.log(`gcloud run deploy ${tag} --project ${projectId} --image gcr.io/${projectId}/${tag} --platform managed --region ${region} ${allowUnauthenticated ? '--allow-unauthenticated' : ''} --service-account ${serviceAccount}`);
