/**
 * @module
 * @description Get Cloud Run URL
 *
 * @license
 * Copyright 2020, TJ Monserrat.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { getConfig } from '../../src/utils/helpers/config.js';

const { google } = getConfig();
const { projectId, tag, region } = google;

console.log(`GCLOUD_RUN_URL=$(gcloud run services describe ${tag} --project ${projectId} --platform managed --region ${region} --format 'value(status.url)')`);
console.log('echo $GCLOUD_RUN_URL');
