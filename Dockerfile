FROM node:lts-buster

ENV NODE_ENV=production

WORKDIR /usr/src/applicable
COPY package*.json ./

RUN npm install --only=production

COPY . .

CMD ["node", "src"]
